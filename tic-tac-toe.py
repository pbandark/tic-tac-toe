d1={1: '  ', 2: '  ', 3: '  ', 4: '  ', 5: '  ', 6: '  ', 7: '  ', 8: '  ', 9: '  ' }


def grid():
    print("---------------------------------\n"
          "|          |          |          |\n"
          "|    {}    |    {}    |    {}    |\n"
          "|          |          |          |\n"
          "---------------------------------\n"
          "|          |          |          |\n"
          "|    {}    |    {}    |    {}    |\n"
          "|          |          |          |\n"
          "----------------------------------\n"
          "|          |          |          |\n"
          "|    {}    |    {}    |    {}    |\n"
          "|          |          |          |\n"
          "---------------------------------".format(d1[7],d1[8],d1[9],d1[4],d1[5],d1[6],d1[1],d1[2],d1[3])
          )


def user_input(player):
    if '  ' in d1.values():
        while True:
            value=int(input("Choose your next position: (1-9)"))
            if value in range(1,10):
                if d1[value] == '  ':
                    d1[value] = player+" "
                    grid()
                    if ((d1[1] == d1[2] == d1[3]) and ((d1[1] or d1[2] or d1[3]) != '  ')) or\
                        ((d1[4] == d1[5] == d1[6]) and ((d1[4] or d1[5] or d1[6]) != '  ')) or \
                        ((d1[7] == d1[8] == d1[9]) and ((d1[7] or d1[8] or d1[9]) != '  ')) or \
                        ((d1[1] == d1[4] == d1[7]) and ((d1[1] or d1[4] or d1[7]) != '  ')) or \
                        ((d1[2] == d1[5] == d1[8]) and ((d1[2] or d1[5] or d1[8]) != '  ')) or \
                        ((d1[3] == d1[6] == d1[9]) and ((d1[3] or d1[6] or d1[9]) != '  ')) or \
                        ((d1[1] == d1[5] == d1[9]) and ((d1[1] or d1[5] or d1[9]) != '  ')) or \
                        ((d1[3] == d1[5] == d1[7]) and ((d1[3] or d1[5] or d1[7]) != '  ')):
                        print("Congratulations! {} won the game".format(players[player].capitalize()))
                        exit()
                    return
                else:
                    print("ERROR: Invalid selection")
                    continue
            else:
                print("Invalid Input")
                break
    else:
        print("It's a tie. It seems you both are genius ;-)")
        exit()


print("Welcome to Tic Tac Toe!")
while True:
    player1 = str(input("Player 1: Do you want to be X or O?"))
    if player1 == "X":
        player2 = "O"
        break
    elif player1 == "O":
        player2 = "X"
        break
    else:
        print("Please select valid input")
        continue

players = {player1: 'player1', player2: 'player2'}
print("Player1={}, Player2={}".format(player1, player2))

print("Player1, you will go first. Please make your move")
while True:
        print("Player 1 turn:")
        user_input(player1)
        print("Player 2 turn:")
        user_input(player2)

